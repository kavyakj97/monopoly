let board = [
  new cell("empty", landOnEmptyCell),
  new cell("jail", landOnJailCell),
  new cell("hotel", landOnHotelCell),
  new cell("treasure", landOnTreasureCell),
  new cell("hotel", landOnHotelCell),
  new cell("empty", landOnEmptyCell),
  new cell("jail", landOnJailCell),
  new cell("treasure", landOnTreasureCell)
];

function cell(type, change) {
  this.type = type;
  this.changeAmount = function (player) {
    change(player)
  };
}

function landOnJailCell(player) {
  player.amount -= 150
}
function landOnEmptyCell(player) {
}
function landOnTreasureCell(player) {
  player.amount += 200;
}
function landOnHotelCell(player) {
  if (!checkIfPlayerOwnsHotel(player)) {
    checkIfPlayerBuysOrPaysRent(player);
  }
}

let players = [
  new player(),
  new player()
]

game(players);

function player() {
  this.amount = 2000;
  this.position = 0;
  this.hotelOwnerCells = [];
}

function game(players) {
  let numberOfPlayers = players.length;
  let totalChances = numberOfPlayers * 10;

  for (let i = 1; i <= totalChances; i++) {
    traversal(players[i % numberOfPlayers]);
  }
  players.map(player => console.log(player.amount));
}

function randomNumberGenerator() {
  return Math.floor(Math.random() * (12 - 2 + 1) + 2);
}

function positionOnBoardAfterRoll(currentPosition, size) {
  return currentPosition % size;
}

function cellOnBoard(position) {
  return board[position];
}

function traversal(player) {
  let currentRollValue = randomNumberGenerator();
  player.position = positionOnBoardAfterRoll(currentRollValue + player.position, board.length);
  let currentCell = cellOnBoard(player.position);
  currentCell.changeAmount(player);
}

function checkIfPlayerOwnsHotel(player) {
  return player.hotelOwnerCells.includes(player.position);
}

function checkIfPlayerBuysOrPaysRent(player) {
  var owner = preOwnedHotel(player.position);
  if (owner) {
    owner.amount += 50;
    player.amount -= 50;
  }
  else if (!owner && player.amount > 200) {
    player.hotelOwnerCells.push(player.position);
    player.amount -= 200;
  }
}

function preOwnedHotel(cellPosition) {
  for (let i = 0; i < players.length; i++) {
    var otherPlayer = players[i];
    if (otherPlayer.hotelOwnerCells.includes(cellPosition)) {
      return otherPlayer;
    }
  }
  return null;
}

module.exports = {
  player,
  cellOnBoard,
  positionOnBoardAfterRoll,
}
